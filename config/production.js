var config = {
    appName: 'Geddy App', detailedErrors: false, hostname: null, port: 4000, model: {
        defaultAdapter: 'mongo'
    }, db: {
        mongo: {
            username: null, dbname: 'production', prefix: null, password: null, host: 'localhost', port: 27017
        }
    }
};
module.exports = config;