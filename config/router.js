var router = new geddy.RegExpRouter();
router.get('/').to('Students.index');
router.resource('students');
exports.router = router;