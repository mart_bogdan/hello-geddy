var Students = function () {
    this.respondsWith = ['html'];
    this.index = function (req, resp, params) {
        var self = this;
        geddy.model.Student.all(function (err, students) {
            if (err) {
                throw err;
            }
            self.respondWith(students, {type: 'Student'});
        });
    };
    this.add = function (req, resp, params) {
        this.respond({params: params});
    };
    this.create = function (req, resp, params) {
        var self = this, student = geddy.model.Student.create(params);
        if (!student.isValid()) {
            this.respondWith(student);
        }
        else {
            student.save(function (err, data) {
                if (err) {
                    throw err;
                }
                self.respondWith(student, {status: err});
            });
        }
    };
    this.show = function (req, resp, params) {
        var self = this;
        geddy.model.Student.first(params.id, function (err, student) {
            if (err) {
                throw err;
            }
            if (!student) {
                throw new geddy.errors.NotFoundError();
            }
            else {
                self.respondWith(student);
            }
        });
    };
    this.edit = function (req, resp, params) {
        var self = this;
        geddy.model.Student.first(params.id, function (err, student) {
            if (err) {
                throw err;
            }
            if (!student) {
                throw new geddy.errors.BadRequestError();
            }
            else {
                self.respondWith(student);
            }
        });
    };
    this.update = function (req, resp, params) {
        var self = this;
        geddy.model.Student.first(params.id, function (err, student) {
            if (err) {
                throw err;
            }
            student.updateProperties(params);
            if (!student.isValid()) {
                self.respondWith(student);
            }
            else {
                student.save(function (err, data) {
                    if (err) {
                        throw err;
                    }
                    self.respondWith(student, {status: err});
                });
            }
        });
    };
    this.remove = function (req, resp, params) {
        var self = this;
        geddy.model.Student.first(params.id, function (err, student) {
            if (err) {
                throw err;
            }
            if (!student) {
                throw new geddy.errors.BadRequestError();
            }
            else {
                geddy.model.Student.remove(params.id, function (err) {
                    if (err) {
                        throw err;
                    }
                    self.respondWith(student);
                });
            }
        });
    };
};
exports.Students = Students;